import * as React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import LoginButton from './LoginButton';
import {
  Avatar,
  LogOutIcon,
  Menu,
  Popover,
  Position,
  Pane,
  UserIcon,
} from 'evergreen-ui';
import { useHistory } from 'react-router-dom';

const Auth0LoginWidget = () => {
  const { user, isAuthenticated, isLoading, logout } = useAuth0();
  const history = useHistory();

  if (isLoading) {
    return <div>Loading...</div>;
  }

  return isAuthenticated ? (
    <Popover
      position={Position.BOTTOM_RIGHT}
      content={({ close }) => (
        <Menu>
          <Menu.Group>
            <Menu.Item
              icon={UserIcon}
              onSelect={() => {
                history.push('/profile');
                close();
              }}
            >
              My Profile
            </Menu.Item>
            <Menu.Item
              icon={LogOutIcon}
              onSelect={() => logout({ returnTo: window.location.origin })}
            >
              Logout
            </Menu.Item>
          </Menu.Group>
        </Menu>
      )}
    >
      <Pane>
        <Avatar name={user.name} src={user.picture} size={40} />
      </Pane>
    </Popover>
  ) : (
    <LoginButton />
  );
};

export default Auth0LoginWidget;
