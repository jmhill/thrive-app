import React from 'react';
import { Route } from 'react-router-dom';
import { withAuthenticationRequired } from '@auth0/auth0-react';

const Loading = () => <div>Loading... Please wait.</div>;

const ProtectedRoute = ({
  component,
  ...args
}: {
  component: any;
  path: any;
}) => (
  <Route
    component={withAuthenticationRequired(component, {
      onRedirecting: () => <Loading />,
    })}
    {...args}
  />
);

export default ProtectedRoute;
