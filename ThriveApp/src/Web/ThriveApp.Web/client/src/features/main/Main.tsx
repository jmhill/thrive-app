import * as React from 'react';
import { Counter } from '../counter/Counter';
import { ProtectedRoute } from '../login';
import { ForecastDemo } from '../forecast-demo/ForecastDemo';
import { Switch, Route } from 'react-router-dom';

const Main = () => (
  <main>
    <Switch>
      <Route path="/counter" component={Counter} />
      <ProtectedRoute path="/forecast" component={ForecastDemo} />
      <ProtectedRoute path="/profile" component={Profile} />
      <Route path="/" component={Home} />
    </Switch>
  </main>
);

function Home() {
  return <div>Home</div>;
}

function Profile() {
  return <div>Profile</div>;
}

export default Main;
