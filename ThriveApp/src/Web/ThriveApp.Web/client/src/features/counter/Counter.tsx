import React, { useState } from 'react';
import { Button, Card, Pane, Text, TextInput } from 'evergreen-ui';
import { useSelector, useDispatch } from 'react-redux';
import {
  decrement,
  increment,
  incrementByAmount,
  incrementAsync,
  selectCount,
} from './counterSlice';

export function Counter() {
  const count = useSelector(selectCount);
  const dispatch = useDispatch();
  const [incrementAmount, setIncrementAmount] = useState('');

  return (
    <Card
      background="blueTint"
      elevation={2}
      padding={8}
      display="flex"
      flexDirection="column"
      alignItems="center"
    >
      <Pane padding={8} display="flex" alignItems="center">
        <Button
          appearance="primary"
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          -
        </Button>
        <Text size={600} marginX={16}>
          {count}
        </Text>
        <Button
          appearance="primary"
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          +
        </Button>
      </Pane>
      <Pane padding={8}>
        <TextInput
          width={220}
          marginX={16}
          placeholder="Set increment amount"
          aria-label="Set increment amount"
          value={incrementAmount}
          onChange={(e: any) => setIncrementAmount(e.target.value)}
        />
      </Pane>
      <Pane padding={8}>
        <Button
          marginX={16}
          onClick={() =>
            dispatch(incrementByAmount(Number(incrementAmount) || 0))
          }
        >
          Add Amount
        </Button>
        <Button
          marginX={16}
          onClick={() => dispatch(incrementAsync(Number(incrementAmount) || 0))}
        >
          Add Async
        </Button>
      </Pane>
    </Card>
  );
}
