import React, { useState } from 'react';
import { useAuth0 } from '@auth0/auth0-react';

export const ForecastDemo = () => {
  const [data, setData] = useState('');
  const apiUrl = process.env.REACT_APP_API_SERVER_URL;

  const { getAccessTokenSilently } = useAuth0();

  const callApi = async () => {
    try {
      const token = await getAccessTokenSilently();
      const response = await fetch(`${apiUrl}/WeatherForecast`, {
        headers: { Authorization: `Bearer ${token}` },
      });
      const responseData = await response.json();
      const formatted = responseData
        .map(
          (item: { date: Date; summary: string }) =>
            `Day: ${new Date(item.date).toLocaleDateString()} Summary: ${
              item.summary
            }`
        )
        .join(' | ');
      setData(formatted);
    } catch (err) {
      setData(err.message);
    }
  };

  return (
    <div>
      <button type="button" onClick={callApi}>
        Fetch Forecast Demo Data
      </button>
      <div>{data}</div>
    </div>
  );
};
