import * as React from 'react';
import { Pane, Heading, TabNavigation, Tab } from 'evergreen-ui';
import { Link } from 'react-router-dom';
import { Auth0UserMenu } from '../login';

const NavBar = () => {
  return (
    <Pane paddingTop={24} paddingX={24} background="tint2" elevation={1}>
      <Pane display="flex" height={40}>
        <Pane flex={1} display="flex" marginLeft={8}>
          <Heading size={600}>ThriveApp</Heading>
        </Pane>
        <Pane>
          <Auth0UserMenu />
        </Pane>
      </Pane>
      <TabNavigation>
        <Tab>
          <Link to="/">Home</Link>
        </Tab>
        <Tab>
          <Link to="/counter">Counter</Link>
        </Tab>
        <Tab>
          <Link to="/forecast">Forecast Demo</Link>
        </Tab>
      </TabNavigation>
    </Pane>
  );
};

export default NavBar;
