import * as React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import NavBar from './features/navigation/NavBar';
import Main from './features/main/Main';
import { Pane } from 'evergreen-ui';
function App() {
  return (
    <Router>
      <Pane height="100vh">
        <NavBar />
        <Pane
          display="flex"
          flexDirection="column"
          alignItems="center"
          padding={16}
        >
          <Main />
        </Pane>
      </Pane>
    </Router>
  );
}

export default App;
