﻿using Ardalis.Specification;
using ThriveApp.Core.Entities;

namespace ThriveApp.Core.Specifications
{
    public class IncompleteItemsSpecification : Specification<ToDoItem>
    {
        public IncompleteItemsSpecification()
        {
            Query.Where(item => !item.IsDone);
        }
    }
}
