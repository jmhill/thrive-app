using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace ThriveApp.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SmokeTestController : ControllerBase
    {
        private readonly ILogger<SmokeTestController> _logger;

        public SmokeTestController(ILogger<SmokeTestController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            return "It works!";
        }

    }
}
